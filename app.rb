#!/usr/bin/env ruby

require 'fileutils'
require 'nokogiri'
require 'open-uri'
require 'trollop'
require 'logger'

include FileUtils

## PREP
logger = Logger.new($stdout)

opts = Trollop::options do
   opt :verbose, "Enable verbosity", default: false
   opt :debug, "Enable debug mode", default: false
   opt :threads, "Maximum number of threads", type: :integer, default: 10
   opt :output, "Output directory", type: :string, default: "out"
end

# only select one log level, start at the highest
if opts[:debug]
   logger.level = Logger::DEBUG
elsif opts[:verbose]
   logger.level = Logger::INFO
else
   logger.level = Logger::WARN
end

# create the directory in case it doesn't exist
mkdir_p opts[:output]

## ERROR HANDLING
if ! File.exist?(ARGV[0] || "") && ! File.exist?("urls.list")
   raise "ERROR: File doesn't exist: #{ARGV[0]}"
end

# assuming either the file exists or urls.list does,
# determine which one we want to use
file_name = ARGV[0].nil? ? "urls.list" : ARGV[0]

## GLOBALS
collection_urls = File.read(file_name).chomp.split("\n")
collection_xpath = "//div[contains(@class,'standalone series')]/ul/li/a"

scip_data = []
scip_xpath = "//div[@id='page-content']"

## PROCESSING
collection_urls.each do |collection|
   # run it by series
   logger.info("Hitting #{collection}")
   
   doc = Nokogiri::HTML(open(collection))

   scips = doc.xpath(collection_xpath).collect do |node|
      {
         title: node.parent.text.strip.downcase.gsub(' ', '_').gsub(/[\/'"]/, ''),
         url: "http://www.scp-wiki.net#{node['href']}"
      }
   end

   scip_data += scips
end

threads = 0

scip_data.each do |scip|
   logger.info(scip)

   # we throttle a little bit here to keep us from looking like a DDOS attack
   if threads >= opts[:threads]
      until threads == 0
      end
   end

   threads += 1
   Thread.new do
      begin
         doc = Nokogiri::HTML(open(scip[:url]))

         File.open(opts[:output] + "/#{scip[:title]}.txt", 'w').write(
            doc.xpath(scip_xpath).text.gsub("Description:", "\n\nDescription:")
         )
      rescue Exception => e
         # a LOT of SCP entries are just stubs
         # until the actual stories are up
         logger.warn("Could not open #{scip[:url]}: #{e}")
      end

      # ever used Golang?
      # this is a WaitGroup
      # without method calls
      threads -= 1
   end
end

until threads == 0
   logger.info("Waiting for all threads to exit...")
end
