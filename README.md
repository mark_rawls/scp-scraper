```bash
git clone git@bitbucket.org:mark_rawls/scp-scraper.git
cd scp-scraper
bundle install

# Options:
#  -v, --verbose        Enable verbosity
#  -d, --debug          Enable debug mode
#  -t, --threads=<i>    Maximum number of threads (default: 10)
#  -o, --output=<s>     Output directory (default: out)
#  -h, --help           Show this message
```
